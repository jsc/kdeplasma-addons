# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Mincho Kondarev <mkondarev@yahoo.de>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-30 08:46+0000\n"
"PO-Revision-Date: 2023-01-07 18:09+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/main.qml:32
#, kde-format
msgid "Night Color Control"
msgstr "Контрол на Вечерни цветове"

#: package/contents/ui/main.qml:35
#, kde-format
msgid "Night Color is inhibited"
msgstr "Вечерни цветове е спрян"

#: package/contents/ui/main.qml:38
#, kde-format
msgid "Night Color is unavailable"
msgstr "Вечерни цветове не е наличен"

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Night Color is disabled. Click to configure"
msgstr "Вечерни цветове е деактивиран. Кликнете за настройка."

#: package/contents/ui/main.qml:44
#, kde-format
msgid "Night Color is not running"
msgstr "Вечерни цветове не работи"

#: package/contents/ui/main.qml:46
#, kde-format
msgid "Night Color is active (%1K)"
msgstr "Вечерни цветове е активен (%1K)"

#: package/contents/ui/main.qml:97
#, kde-format
msgid "&Configure Night Color…"
msgstr "&Конфигуриране на вечерни цветове…"
