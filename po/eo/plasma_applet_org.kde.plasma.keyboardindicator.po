# Translation of plasma_applet_org.kde.plasma.keyboardindicator.pot into esperanto.
# Copyright (C) 2018 Free Software Foundation, Inc.
# This file is distributed under the same license as the kdeplasma-addons package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 02:24+0000\n"
"PO-Revision-Date: 2023-03-26 18:35+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Klavoj"

#: contents/ui/configAppearance.qml:34
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "Montru kiam aktivigita:"

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Majuskloŝlosilo"

#: contents/ui/configAppearance.qml:44
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Numŝlosilo"

#: contents/ui/main.qml:31
#, kde-format
msgid "Caps Lock activated\n"
msgstr "Majuskloŝlosilo aktivigita\n"

#: contents/ui/main.qml:32
#, kde-format
msgid "Num Lock activated\n"
msgstr "Numŝlosilo aktivigita\n"

#: contents/ui/main.qml:112
#, kde-format
msgid "No lock keys activated"
msgstr "Neniuj ŝlosiloj aktivigitaj"
