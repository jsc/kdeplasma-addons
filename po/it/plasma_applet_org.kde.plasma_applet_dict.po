# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Paolo Zamponi <zapaolo@email.it>, 2018, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-31 00:48+0000\n"
"PO-Revision-Date: 2022-05-28 07:49+0200\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.1\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Dictionaries"
msgstr "Dizionari"

#: package/contents/ui/AvailableDictSheet.qml:35
#, kde-format
msgid "Add More Dictionaries"
msgstr "Aggiungi altri dizionari"

#: package/contents/ui/ConfigDictionaries.qml:89
#, kde-format
msgid "Unable to load dictionary list"
msgstr "Impossibile caricare l'elenco dei dizionari"

#: package/contents/ui/ConfigDictionaries.qml:90
#: package/contents/ui/main.qml:121
#, kde-format
msgctxt "%2 human-readable error string"
msgid "Error code: %1 (%2)"
msgstr "Codice di errore: %1 (%2)"

#: package/contents/ui/ConfigDictionaries.qml:101
#, kde-format
msgid "No dictionaries"
msgstr "Nessun dizionario"

#: package/contents/ui/ConfigDictionaries.qml:112
#, kde-format
msgid "Add More…"
msgstr "Aggiungine altri..."

#: package/contents/ui/DictItemDelegate.qml:51
#, kde-format
msgid "Delete"
msgstr "Elimina"

#: package/contents/ui/main.qml:40
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter word to define here…"
msgstr "Inserisci qui una parola da definire…"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Unable to load definition"
msgstr "Impossibile caricare la definizione"

#~ msgid "Looking up definition…"
#~ msgstr "Ricerca delle definizioni…"
