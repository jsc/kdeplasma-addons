# Translation of plasma_applet_weather into Japanese.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2008, 2009, 2010.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2021.
# R.Suga <21r.suga@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_weather\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 02:24+0000\n"
"PO-Revision-Date: 2022-03-06 11:56+0900\n"
"Last-Translator: R.Suga <21r.suga@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 21.12.3\n"

#: i18n.dat:1
#, kde-format
msgctxt "wind direction"
msgid "N"
msgstr "北"

#: i18n.dat:2
#, kde-format
msgctxt "wind direction"
msgid "NNE"
msgstr "北北東"

#: i18n.dat:3
#, kde-format
msgctxt "wind direction"
msgid "NE"
msgstr "北東"

#: i18n.dat:4
#, kde-format
msgctxt "wind direction"
msgid "ENE"
msgstr "東北東"

#: i18n.dat:5
#, kde-format
msgctxt "wind direction"
msgid "E"
msgstr "東"

#: i18n.dat:6
#, kde-format
msgctxt "wind direction"
msgid "SSE"
msgstr "南南東"

#: i18n.dat:7
#, kde-format
msgctxt "wind direction"
msgid "SE"
msgstr "南東"

#: i18n.dat:8
#, kde-format
msgctxt "wind direction"
msgid "ESE"
msgstr "東南東"

#: i18n.dat:9
#, kde-format
msgctxt "wind direction"
msgid "S"
msgstr "南"

#: i18n.dat:10
#, kde-format
msgctxt "wind direction"
msgid "NNW"
msgstr "北北西"

#: i18n.dat:11
#, kde-format
msgctxt "wind direction"
msgid "NW"
msgstr "北西"

#: i18n.dat:12
#, kde-format
msgctxt "wind direction"
msgid "WNW"
msgstr "西北西"

#: i18n.dat:13
#, kde-format
msgctxt "wind direction"
msgid "W"
msgstr "西"

#: i18n.dat:14
#, kde-format
msgctxt "wind direction"
msgid "SSW"
msgstr "南南東"

#: i18n.dat:15
#, kde-format
msgctxt "wind direction"
msgid "SW"
msgstr "南西"

#: i18n.dat:16
#, kde-format
msgctxt "wind direction"
msgid "WSW"
msgstr "西南西"

#: i18n.dat:17
#, kde-format
msgctxt "wind direction"
msgid "VR"
msgstr "VR"

#: i18n.dat:18
#, kde-format
msgctxt "wind speed"
msgid "Calm"
msgstr "無風"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Weather Station"
msgstr "測候所"

#: package/contents/config/config.qml:19
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "外観"

#: package/contents/config/config.qml:25
#, kde-format
msgctxt "@title"
msgid "Units"
msgstr "単位"

#: package/contents/ui/config/ConfigAppearance.qml:36
#, kde-format
msgctxt "@title:group"
msgid "Compact Mode"
msgstr "コンパクトモード"

#: package/contents/ui/config/ConfigAppearance.qml:44
#, fuzzy, kde-format
#| msgctxt "@label:listbox"
#| msgid "Temperature:"
msgctxt "@label"
msgid "Show temperature:"
msgstr "気温:"

#: package/contents/ui/config/ConfigAppearance.qml:47
#, fuzzy, kde-format
#| msgctxt "@label"
#| msgid "Show beside widget icon:"
msgctxt "@option:radio Show temperature:"
msgid "Over the widget icon"
msgstr "ウィジェットアイコンの横に表示:"

#: package/contents/ui/config/ConfigAppearance.qml:55
#, fuzzy, kde-format
#| msgctxt "@label"
#| msgid "Show beside widget icon:"
msgctxt "@option:radio Show temperature:"
msgid "Beside the widget icon"
msgstr "ウィジェットアイコンの横に表示:"

#: package/contents/ui/config/ConfigAppearance.qml:62
#, kde-format
msgctxt "@option:radio Show temperature:"
msgid "Do not show"
msgstr ""

#: package/contents/ui/config/ConfigAppearance.qml:71
#, kde-format
msgctxt "@label"
msgid "Show in tooltip:"
msgstr "ツールチップ内に表示:"

#: package/contents/ui/config/ConfigAppearance.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Temperature"
msgstr "気温"

#: package/contents/ui/config/ConfigAppearance.qml:77
#, kde-format
msgctxt "@option:check Show in tooltip: wind"
msgid "Wind"
msgstr "風"

#: package/contents/ui/config/ConfigAppearance.qml:82
#, kde-format
msgctxt "@option:check Show in tooltip: pressure"
msgid "Pressure"
msgstr "気圧"

#: package/contents/ui/config/ConfigAppearance.qml:87
#, kde-format
msgctxt "@option:check Show in tooltip: humidity"
msgid "Humidity"
msgstr "湿度"

#: package/contents/ui/config/ConfigUnits.qml:33
#, kde-format
msgctxt "@label:listbox"
msgid "Temperature:"
msgstr "気温:"

#: package/contents/ui/config/ConfigUnits.qml:39
#, kde-format
msgctxt "@label:listbox"
msgid "Pressure:"
msgstr "気圧:"

#: package/contents/ui/config/ConfigUnits.qml:45
#, kde-format
msgctxt "@label:listbox"
msgid "Wind speed:"
msgstr "風速:"

#: package/contents/ui/config/ConfigUnits.qml:51
#, kde-format
msgctxt "@label:listbox"
msgid "Visibility:"
msgstr "視程:"

#: package/contents/ui/config/ConfigWeatherStation.qml:46
#, kde-format
msgctxt "@label:spinbox"
msgid "Update every:"
msgstr "更新頻度:"

#: package/contents/ui/config/ConfigWeatherStation.qml:49
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 分"
msgstr[1] "%1 分"

#: package/contents/ui/config/ConfigWeatherStation.qml:61
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "場所:"

#: package/contents/ui/config/ConfigWeatherStation.qml:67
#, kde-format
msgctxt "No location is currently selected"
msgid "None selected"
msgstr ""

#: package/contents/ui/config/ConfigWeatherStation.qml:71
#, kde-format
msgctxt "@label"
msgid "Provider:"
msgstr ""

#: package/contents/ui/config/ConfigWeatherStation.qml:95
#, fuzzy, kde-format
#| msgctxt "@info:placeholder"
#| msgid "Enter location"
msgctxt "@info:placeholder"
msgid "Enter new location"
msgstr "場所を入力"

#: package/contents/ui/config/ConfigWeatherStation.qml:95
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter location"
msgstr "場所を入力"

#: package/contents/ui/config/ConfigWeatherStation.qml:188
#, kde-format
msgctxt "@info"
msgid "No weather stations found for '%1'"
msgstr "'%1' の測候所が見つかりません"

#: package/contents/ui/config/ConfigWeatherStation.qml:190
#, kde-format
msgctxt "@info"
msgid "Search for a weather station to change your location"
msgstr ""

#: package/contents/ui/config/ConfigWeatherStation.qml:192
#, kde-format
msgctxt "@info"
msgid "Search for a weather station to set your location"
msgstr ""

#: package/contents/ui/ForecastView.qml:73
#: package/contents/ui/ForecastView.qml:80
#, kde-format
msgctxt "Short for no data available"
msgid "-"
msgstr "-"

#: package/contents/ui/FullRepresentation.qml:36
#, kde-format
msgid "Please set your location"
msgstr "現在の場所を設定してください"

#: package/contents/ui/FullRepresentation.qml:39
#, kde-format
msgid "Set location…"
msgstr "場所を設定…"

#: package/contents/ui/FullRepresentation.qml:54
#, kde-format
msgid "Weather information retrieval for %1 timed out."
msgstr "%1 からの気象情報の取得がタイムアウトしました。"

#: package/contents/ui/main.qml:87
#, kde-format
msgctxt "pressure tendency"
msgid "Rising"
msgstr "上昇傾向"

#: package/contents/ui/main.qml:88
#, kde-format
msgctxt "pressure tendency"
msgid "Falling"
msgstr "下降傾向"

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "pressure tendency"
msgid "Steady"
msgstr "安定"

#: package/contents/ui/main.qml:111
#, kde-format
msgctxt "Wind condition"
msgid "Calm"
msgstr "無風"

#: package/contents/ui/main.qml:149
#, kde-format
msgctxt "Forecast period timeframe"
msgid "1 Day"
msgid_plural "%1 Days"
msgstr[0] "1 日"
msgstr[1] "%1 日"

#: package/contents/ui/main.qml:171
#, kde-format
msgctxt "@label"
msgid "Windchill:"
msgstr "体感温度:"

#: package/contents/ui/main.qml:178
#, kde-format
msgctxt "@label"
msgid "Humidex:"
msgstr "不快指数 (Humidex):"

#: package/contents/ui/main.qml:185
#, kde-format
msgctxt "@label ground temperature"
msgid "Dewpoint:"
msgstr "露点:"

#: package/contents/ui/main.qml:192
#, kde-format
msgctxt "@label"
msgid "Pressure:"
msgstr "気圧:"

#: package/contents/ui/main.qml:199
#, kde-format
msgctxt "@label pressure tendency, rising/falling/steady"
msgid "Pressure Tendency:"
msgstr "気圧傾向:"

#: package/contents/ui/main.qml:206
#, kde-format
msgctxt "@label"
msgid "Visibility:"
msgstr "視程:"

#: package/contents/ui/main.qml:213
#, kde-format
msgctxt "@label"
msgid "Humidity:"
msgstr "湿度:"

#: package/contents/ui/main.qml:220
#, kde-format
msgctxt "@label"
msgid "Wind Gust:"
msgstr "突風:"

#: package/contents/ui/main.qml:240
#, fuzzy, kde-format
#| msgctxt "Forecast period timeframe"
#| msgid "1 Day"
#| msgid_plural "%1 Days"
msgctxt "Time of the day (from the duple Day/Night)"
msgid "Day"
msgstr "1 日"

#: package/contents/ui/main.qml:241
#, kde-format
msgctxt "Time of the day (from the duple Day/Night)"
msgid "Night"
msgstr ""

#: package/contents/ui/main.qml:279
#, kde-format
msgctxt "certain weather condition (probability percentage)"
msgid "%1 (%2 %)"
msgstr "%1 (%2 %)"

#: package/contents/ui/main.qml:371
#, kde-format
msgctxt "@info:tooltip %1 is the translated plasmoid name"
msgid "Click to configure %1"
msgstr ""

#: package/contents/ui/main.qml:382
#, kde-format
msgctxt "weather condition + temperature"
msgid "%1 %2"
msgstr "%1 %2"

#: package/contents/ui/main.qml:389
#, kde-format
msgctxt "winddirection windspeed (windgust)"
msgid "%1 %2 (%3)"
msgstr "%1 %2 (%3)"

#: package/contents/ui/main.qml:392
#, kde-format
msgctxt "winddirection windspeed"
msgid "%1 %2"
msgstr "%1 %2"

#: package/contents/ui/main.qml:401
#, kde-format
msgctxt "pressure (tendency)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/main.qml:408
#, kde-format
msgid "Humidity: %1"
msgstr "湿度: %1"

#: package/contents/ui/NoticesView.qml:38
#, kde-format
msgctxt "@title:column weather warnings"
msgid "Warnings Issued"
msgstr "発表中の警報"

#: package/contents/ui/NoticesView.qml:38
#, kde-format
msgctxt "@title:column weather watches"
msgid "Watches Issued"
msgstr "発表中の注意報"

#: package/contents/ui/SwitchPanel.qml:43
#, kde-format
msgctxt "@title:tab"
msgid "Details"
msgstr "詳細"

#: package/contents/ui/SwitchPanel.qml:59
#, kde-format
msgctxt "@title:tab"
msgid "Notices"
msgstr "情報"

#: plugin/locationlistmodel.cpp:41 plugin/locationlistmodel.cpp:69
#, kde-format
msgid "Cannot find '%1' using %2."
msgstr "%2 を使って '%1' を見つけられませんでした。"

#: plugin/locationlistmodel.cpp:66
#, kde-format
msgid "Connection to %1 weather server timed out."
msgstr "%1 天気サーバへの接続がタイムアウトしました。"

#: plugin/locationlistmodel.cpp:125
#, kde-format
msgctxt "A weather station location and the weather service it comes from"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: plugin/util.cpp:43
#, kde-format
msgctxt "Degree, unit symbol"
msgid "°"
msgstr "°"

#: plugin/util.cpp:47 plugin/util.cpp:51
#, kde-format
msgctxt "temperature unitsymbol"
msgid "%1 %2"
msgstr "%1 %2"

#: plugin/util.cpp:61
#, kde-format
msgctxt "value unitsymbol"
msgid "%1 %2"
msgstr "%1 %2"

#: plugin/util.cpp:67
#, kde-format
msgctxt "value percentsymbol"
msgid "%1 %"
msgstr "%1 %"

#: plugin/util.cpp:73
#, kde-format
msgctxt "@item %1 is a unit description and %2 its unit symbol"
msgid "%1 (%2)"
msgstr "%1 (%2)"
