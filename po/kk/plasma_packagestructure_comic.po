# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sairan Kikkarin <sairan@computer.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-25 00:19+0000\n"
"PO-Revision-Date: 2010-04-17 08:50+0600\n"
"Last-Translator: Sairan Kikkarin <sairan@computer.org>\n"
"Language-Team: Kazakh <kde-i18n-doc@kde.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: comic_package.cpp:20
#, kde-format
msgid "Images"
msgstr "Кескіндер"

#: comic_package.cpp:25
#, kde-format
msgid "Executable Scripts"
msgstr "Орындалатын скрипттер"

#: comic_package.cpp:29
#, kde-format
msgid "Main Script File"
msgstr "Негізгі скрипт файлы"
