# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2015, 2017, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-05 03:09+0000\n"
"PO-Revision-Date: 2018-08-14 15:40+0100\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 2.0\n"

#: package/contents/config/config.qml:11
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "일반"

#: package/contents/ui/CompactRepresentation.qml:71
#, kde-format
msgctxt "@info:tooltip"
msgid "Pick color"
msgstr "색상 선택"

#: package/contents/ui/CompactRepresentation.qml:78
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Drag a color code here to save it<nl/>Drag an image file here to get its "
"average color"
msgstr ""

#: package/contents/ui/configGeneral.qml:25
#, kde-format
msgctxt "@label:listbox"
msgid "Default color format:"
msgstr "기본 색상 형식:"

#: package/contents/ui/configGeneral.qml:33
#, kde-format
msgctxt "@option:check"
msgid "Automatically copy color to clipboard"
msgstr "자동으로 클립보드에 색 복사"

#: package/contents/ui/configGeneral.qml:41
#, kde-format
msgctxt "@label"
msgid "When pressing the keyboard shortcut:"
msgstr "키보드 단축키를 눌렀을 때:"

#: package/contents/ui/configGeneral.qml:42
#, kde-format
msgctxt "@option:radio"
msgid "Pick a color"
msgstr "색상 선택"

#: package/contents/ui/configGeneral.qml:48
#, kde-format
msgctxt "@option:radio"
msgid "Show history"
msgstr "과거 기록 보기"

#: package/contents/ui/configGeneral.qml:58
#, kde-format
msgctxt "@label"
msgid "Preview count:"
msgstr ""

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "@action"
msgid "Open Color Dialog"
msgstr "색 대화 상자 열기"

#: package/contents/ui/main.qml:111
#, kde-format
msgctxt "@action"
msgid "Clear History"
msgstr "과거 기록 비우기"

#: package/contents/ui/main.qml:112
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "Clear History"
msgctxt "@action"
msgid "View History"
msgstr "과거 기록 비우기"

#: package/contents/ui/main.qml:114
#, kde-format
msgctxt "@title:menu"
msgid "Copy to Clipboard"
msgstr "클립보드에 복사"

#: package/contents/ui/main.qml:151
#, fuzzy, kde-format
#| msgctxt "@info:tooltip"
#| msgid "Pick color"
msgctxt "@info:usagetip"
msgid "No colors"
msgstr "색상 선택"

#: package/contents/ui/main.qml:155
#, kde-format
msgctxt "@action:button"
msgid "Pick Color"
msgstr "색상 선택"

#: package/contents/ui/main.qml:330
#, kde-format
msgctxt "@info:progress just copied a color to clipboard"
msgid "Copied!"
msgstr ""

#: package/contents/ui/main.qml:340
#, kde-format
msgctxt "@action:button"
msgid "Delete"
msgstr ""

#~ msgctxt "@info:tooltip"
#~ msgid "Color options"
#~ msgstr "색 옵션"

#~ msgid "Default Format"
#~ msgstr "기본 형식"

#~ msgid "Latex Color"
#~ msgstr "LaTeX 색상"
