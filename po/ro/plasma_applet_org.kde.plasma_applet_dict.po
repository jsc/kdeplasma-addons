# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Sergiu Bivol <sergiu@cip.md>, 2020, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-31 00:48+0000\n"
"PO-Revision-Date: 2022-06-01 15:22+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Dictionaries"
msgstr "Dicționare"

#: package/contents/ui/AvailableDictSheet.qml:35
#, kde-format
msgid "Add More Dictionaries"
msgstr "Adaugă dicționare suplimentare"

#: package/contents/ui/ConfigDictionaries.qml:89
#, kde-format
msgid "Unable to load dictionary list"
msgstr "Lista cu dicționare nu poate fi încărcată"

#: package/contents/ui/ConfigDictionaries.qml:90
#: package/contents/ui/main.qml:121
#, kde-format
msgctxt "%2 human-readable error string"
msgid "Error code: %1 (%2)"
msgstr "Cod de eroare: %1 (%2)"

#: package/contents/ui/ConfigDictionaries.qml:101
#, kde-format
msgid "No dictionaries"
msgstr "Niciun dicționar"

#: package/contents/ui/ConfigDictionaries.qml:112
#, kde-format
msgid "Add More…"
msgstr "Adaugă mai multe…"

#: package/contents/ui/DictItemDelegate.qml:51
#, kde-format
msgid "Delete"
msgstr "Șterge"

#: package/contents/ui/main.qml:40
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter word to define here…"
msgstr "Introduceți aici cuvântul de definit…"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Unable to load definition"
msgstr "Definiția nu poate fi încărcată"

#~ msgid "Looking up definition…"
#~ msgstr "Se caută definiția…"
