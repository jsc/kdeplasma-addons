# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>, 2008, 2009.
# Jeff Huang <s8321414@gmail.com>, 2016.
# pan93412 <pan93412@gmail.com>, 2018, 2019, 2020.
# Kisaragi Hiu <mail@kisaragi-hiu.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_weather\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 02:24+0000\n"
"PO-Revision-Date: 2023-01-19 23:46+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Traditional Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: i18n.dat:1
#, kde-format
msgctxt "wind direction"
msgid "N"
msgstr "北"

#: i18n.dat:2
#, kde-format
msgctxt "wind direction"
msgid "NNE"
msgstr "北北東"

#: i18n.dat:3
#, kde-format
msgctxt "wind direction"
msgid "NE"
msgstr "東北"

#: i18n.dat:4
#, kde-format
msgctxt "wind direction"
msgid "ENE"
msgstr "東北東"

#: i18n.dat:5
#, kde-format
msgctxt "wind direction"
msgid "E"
msgstr "東"

#: i18n.dat:6
#, kde-format
msgctxt "wind direction"
msgid "SSE"
msgstr "南南東"

#: i18n.dat:7
#, kde-format
msgctxt "wind direction"
msgid "SE"
msgstr "東南"

#: i18n.dat:8
#, kde-format
msgctxt "wind direction"
msgid "ESE"
msgstr "東南東"

#: i18n.dat:9
#, kde-format
msgctxt "wind direction"
msgid "S"
msgstr "南"

#: i18n.dat:10
#, kde-format
msgctxt "wind direction"
msgid "NNW"
msgstr "北北西"

#: i18n.dat:11
#, kde-format
msgctxt "wind direction"
msgid "NW"
msgstr "西北"

#: i18n.dat:12
#, kde-format
msgctxt "wind direction"
msgid "WNW"
msgstr "西北西"

#: i18n.dat:13
#, kde-format
msgctxt "wind direction"
msgid "W"
msgstr "西"

#: i18n.dat:14
#, kde-format
msgctxt "wind direction"
msgid "SSW"
msgstr "南南西"

#: i18n.dat:15
#, kde-format
msgctxt "wind direction"
msgid "SW"
msgstr "西南"

#: i18n.dat:16
#, kde-format
msgctxt "wind direction"
msgid "WSW"
msgstr "西南西"

#: i18n.dat:17
#, kde-format
msgctxt "wind direction"
msgid "VR"
msgstr "VR"

#: i18n.dat:18
#, kde-format
msgctxt "wind speed"
msgid "Calm"
msgstr "涼"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Weather Station"
msgstr "氣象站"

#: package/contents/config/config.qml:19
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "個人化"

#: package/contents/config/config.qml:25
#, kde-format
msgctxt "@title"
msgid "Units"
msgstr "單位"

#: package/contents/ui/config/ConfigAppearance.qml:36
#, kde-format
msgctxt "@title:group"
msgid "Compact Mode"
msgstr "緊湊模式"

#: package/contents/ui/config/ConfigAppearance.qml:44
#, kde-format
msgctxt "@label"
msgid "Show temperature:"
msgstr "顯示溫度："

#: package/contents/ui/config/ConfigAppearance.qml:47
#, kde-format
msgctxt "@option:radio Show temperature:"
msgid "Over the widget icon"
msgstr "蓋在元件圖示上"

#: package/contents/ui/config/ConfigAppearance.qml:55
#, kde-format
msgctxt "@option:radio Show temperature:"
msgid "Beside the widget icon"
msgstr "在元件圖示旁邊"

#: package/contents/ui/config/ConfigAppearance.qml:62
#, kde-format
msgctxt "@option:radio Show temperature:"
msgid "Do not show"
msgstr "不顯示"

#: package/contents/ui/config/ConfigAppearance.qml:71
#, kde-format
msgctxt "@label"
msgid "Show in tooltip:"
msgstr "在工具提示中顯示："

#: package/contents/ui/config/ConfigAppearance.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Temperature"
msgstr "溫度"

#: package/contents/ui/config/ConfigAppearance.qml:77
#, kde-format
msgctxt "@option:check Show in tooltip: wind"
msgid "Wind"
msgstr "風"

#: package/contents/ui/config/ConfigAppearance.qml:82
#, kde-format
msgctxt "@option:check Show in tooltip: pressure"
msgid "Pressure"
msgstr "氣壓"

#: package/contents/ui/config/ConfigAppearance.qml:87
#, kde-format
msgctxt "@option:check Show in tooltip: humidity"
msgid "Humidity"
msgstr "濕度"

#: package/contents/ui/config/ConfigUnits.qml:33
#, kde-format
msgctxt "@label:listbox"
msgid "Temperature:"
msgstr "溫度："

#: package/contents/ui/config/ConfigUnits.qml:39
#, kde-format
msgctxt "@label:listbox"
msgid "Pressure:"
msgstr "氣壓： "

#: package/contents/ui/config/ConfigUnits.qml:45
#, kde-format
msgctxt "@label:listbox"
msgid "Wind speed:"
msgstr "風速："

#: package/contents/ui/config/ConfigUnits.qml:51
#, kde-format
msgctxt "@label:listbox"
msgid "Visibility:"
msgstr "能見度："

#: package/contents/ui/config/ConfigWeatherStation.qml:46
#, kde-format
msgctxt "@label:spinbox"
msgid "Update every:"
msgstr "更新間隔："

#: package/contents/ui/config/ConfigWeatherStation.qml:49
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 分鐘"

#: package/contents/ui/config/ConfigWeatherStation.qml:61
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "位置："

#: package/contents/ui/config/ConfigWeatherStation.qml:67
#, kde-format
msgctxt "No location is currently selected"
msgid "None selected"
msgstr "未選擇"

#: package/contents/ui/config/ConfigWeatherStation.qml:71
#, kde-format
msgctxt "@label"
msgid "Provider:"
msgstr "提供者："

#: package/contents/ui/config/ConfigWeatherStation.qml:95
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter new location"
msgstr "輸入新位置"

#: package/contents/ui/config/ConfigWeatherStation.qml:95
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter location"
msgstr "輸入位置"

#: package/contents/ui/config/ConfigWeatherStation.qml:188
#, kde-format
msgctxt "@info"
msgid "No weather stations found for '%1'"
msgstr "找不到 %1 的天氣站 "

#: package/contents/ui/config/ConfigWeatherStation.qml:190
#, kde-format
msgctxt "@info"
msgid "Search for a weather station to change your location"
msgstr "搜尋氣象站來變更您的位置"

#: package/contents/ui/config/ConfigWeatherStation.qml:192
#, kde-format
msgctxt "@info"
msgid "Search for a weather station to set your location"
msgstr "搜尋氣象站來設定您的位置"

#: package/contents/ui/ForecastView.qml:73
#: package/contents/ui/ForecastView.qml:80
#, kde-format
msgctxt "Short for no data available"
msgid "-"
msgstr "-"

#: package/contents/ui/FullRepresentation.qml:36
#, kde-format
msgid "Please set your location"
msgstr "請設定您的位置"

#: package/contents/ui/FullRepresentation.qml:39
#, kde-format
msgid "Set location…"
msgstr "設定位置…"

#: package/contents/ui/FullRepresentation.qml:54
#, kde-format
msgid "Weather information retrieval for %1 timed out."
msgstr "查詢 %1 的天氣資訊時超過等待時間。"

#: package/contents/ui/main.qml:87
#, kde-format
msgctxt "pressure tendency"
msgid "Rising"
msgstr "上升"

#: package/contents/ui/main.qml:88
#, kde-format
msgctxt "pressure tendency"
msgid "Falling"
msgstr "下降"

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "pressure tendency"
msgid "Steady"
msgstr "穩定"

#: package/contents/ui/main.qml:111
#, kde-format
msgctxt "Wind condition"
msgid "Calm"
msgstr "涼"

#: package/contents/ui/main.qml:149
#, kde-format
msgctxt "Forecast period timeframe"
msgid "1 Day"
msgid_plural "%1 Days"
msgstr[0] "%1 天"

#: package/contents/ui/main.qml:171
#, kde-format
msgctxt "@label"
msgid "Windchill:"
msgstr "風力："

#: package/contents/ui/main.qml:178
#, kde-format
msgctxt "@label"
msgid "Humidex:"
msgstr "濕度："

#: package/contents/ui/main.qml:185
#, kde-format
msgctxt "@label ground temperature"
msgid "Dewpoint:"
msgstr "結露指數："

#: package/contents/ui/main.qml:192
#, kde-format
msgctxt "@label"
msgid "Pressure:"
msgstr "氣壓："

#: package/contents/ui/main.qml:199
#, kde-format
msgctxt "@label pressure tendency, rising/falling/steady"
msgid "Pressure Tendency:"
msgstr "壓力趨勢："

#: package/contents/ui/main.qml:206
#, kde-format
msgctxt "@label"
msgid "Visibility:"
msgstr "能見度："

#: package/contents/ui/main.qml:213
#, kde-format
msgctxt "@label"
msgid "Humidity:"
msgstr "濕度："

#: package/contents/ui/main.qml:220
#, kde-format
msgctxt "@label"
msgid "Wind Gust:"
msgstr "風速："

#: package/contents/ui/main.qml:240
#, kde-format
msgctxt "Time of the day (from the duple Day/Night)"
msgid "Day"
msgstr "白天"

#: package/contents/ui/main.qml:241
#, kde-format
msgctxt "Time of the day (from the duple Day/Night)"
msgid "Night"
msgstr "夜晚"

#: package/contents/ui/main.qml:279
#, kde-format
msgctxt "certain weather condition (probability percentage)"
msgid "%1 (%2 %)"
msgstr "%1 (%2 %)"

#: package/contents/ui/main.qml:371
#, kde-format
msgctxt "@info:tooltip %1 is the translated plasmoid name"
msgid "Click to configure %1"
msgstr "點擊設定%1"

#: package/contents/ui/main.qml:382
#, kde-format
msgctxt "weather condition + temperature"
msgid "%1 %2"
msgstr "%1 %2"

#: package/contents/ui/main.qml:389
#, kde-format
msgctxt "winddirection windspeed (windgust)"
msgid "%1 %2 (%3)"
msgstr "%1 %2 (%3)"

#: package/contents/ui/main.qml:392
#, kde-format
msgctxt "winddirection windspeed"
msgid "%1 %2"
msgstr "%1 %2"

#: package/contents/ui/main.qml:401
#, kde-format
msgctxt "pressure (tendency)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/main.qml:408
#, kde-format
msgid "Humidity: %1"
msgstr "濕度：%1"

#: package/contents/ui/NoticesView.qml:38
#, kde-format
msgctxt "@title:column weather warnings"
msgid "Warnings Issued"
msgstr "已發布的警報"

#: package/contents/ui/NoticesView.qml:38
#, kde-format
msgctxt "@title:column weather watches"
msgid "Watches Issued"
msgstr "已發布的特報"

#: package/contents/ui/SwitchPanel.qml:43
#, kde-format
msgctxt "@title:tab"
msgid "Details"
msgstr "詳細資料"

#: package/contents/ui/SwitchPanel.qml:59
#, kde-format
msgctxt "@title:tab"
msgid "Notices"
msgstr "提醒"

#: plugin/locationlistmodel.cpp:41 plugin/locationlistmodel.cpp:69
#, kde-format
msgid "Cannot find '%1' using %2."
msgstr "無法搜尋到「%1」，改使用「%2」。"

#: plugin/locationlistmodel.cpp:66
#, kde-format
msgid "Connection to %1 weather server timed out."
msgstr "連結至 %1 天氣伺服器時超出等待時間。"

#: plugin/locationlistmodel.cpp:125
#, kde-format
msgctxt "A weather station location and the weather service it comes from"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: plugin/util.cpp:43
#, kde-format
msgctxt "Degree, unit symbol"
msgid "°"
msgstr "°"

#: plugin/util.cpp:47 plugin/util.cpp:51
#, kde-format
msgctxt "temperature unitsymbol"
msgid "%1 %2"
msgstr "%1 %2"

#: plugin/util.cpp:61
#, kde-format
msgctxt "value unitsymbol"
msgid "%1 %2"
msgstr "%1 %2"

#: plugin/util.cpp:67
#, kde-format
msgctxt "value percentsymbol"
msgid "%1 %"
msgstr "%1 %"

#: plugin/util.cpp:73
#, kde-format
msgctxt "@item %1 is a unit description and %2 its unit symbol"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#, fuzzy
#~| msgctxt "@action:button"
#~| msgid "Choose..."
#~ msgctxt "@action:button"
#~ msgid "Choose…"
#~ msgstr "選擇…"

#~ msgctxt "@title:window"
#~ msgid "Select Weather Station"
#~ msgstr "選擇氣象站"

#~ msgctxt "@action:button"
#~ msgid "Select"
#~ msgstr "選取"

#~ msgctxt "@action:button"
#~ msgid "Cancel"
#~ msgstr "取消"

#~ msgctxt "@option:check Show on widget icon: temperature"
#~ msgid "Temperature"
#~ msgstr "溫度"

#~ msgctxt "@info:tooltip"
#~ msgid "Please configure"
#~ msgstr "請設定"

#~ msgctxt "weather services provider name (id)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "Weather providers:"
#~ msgstr "天氣提供者："

#~ msgctxt "@action:button"
#~ msgid "Search"
#~ msgstr "搜尋"

#~ msgctxt "@action:button"
#~ msgid "Configure..."
#~ msgstr "設定…"

#~ msgctxt "@item"
#~ msgid "Celsius °C"
#~ msgstr "攝氏 °C"

#~ msgctxt "@item"
#~ msgid "Fahrenheit °F"
#~ msgstr "華氏 °F"

#~ msgctxt "@item"
#~ msgid "Kelvin K"
#~ msgstr "絕對溫標 K"

#~ msgctxt "@item"
#~ msgid "Hectopascals hPa"
#~ msgstr "百帕 hPa"

#~ msgctxt "@item"
#~ msgid "Kilopascals kPa"
#~ msgstr "千帕 kPa"

#~ msgctxt "@item"
#~ msgid "Millibars mbar"
#~ msgstr "毫巴 mbar"

#~ msgctxt "@item"
#~ msgid "Inches of Mercury inHg"
#~ msgstr "英吋汞柱 inHg"

#~ msgctxt "@item"
#~ msgid "Meters per Second m/s"
#~ msgstr "公尺/秒 m/s"

#~ msgctxt "@item"
#~ msgid "Kilometers per Hour km/h"
#~ msgstr "公里/小時 km/h"

#~ msgctxt "@item"
#~ msgid "Miles per Hour mph"
#~ msgstr "英哩/小時 mph"

#~ msgctxt "@item"
#~ msgid "Knots kt"
#~ msgstr "節 kt"

#~ msgctxt "@item"
#~ msgid "Beaufort scale bft"
#~ msgstr "鮑特 bft"

#~ msgctxt "@item"
#~ msgid "Kilometers"
#~ msgstr "公里"

#~ msgctxt "@item"
#~ msgid "Miles"
#~ msgstr "英哩"
